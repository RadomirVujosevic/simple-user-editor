
# Running

git clone https://gitlab.com/RadomirVujosevic/simple-user-editor.git
Open CmakeLists.txt from QTStudio
Run

# QStyleSheets

The current theme is a modified version of Combinear.  
The original theme can be found on https://qss-stock.devsecstudio.com/templates.php  
The current stylsheet is Assets/Combinear.qss  

A few of the elements had small style changes in their individual style sheets  

# Changes from original UI suggestion

Originally states were supposed to be in a Line Edit component  
Since Cities are listed in a combo box, I decided to make State a Qlabel and change its text to reflect the city choice  
Check the file add_user_form.cpp, functions setup_cb_city and new_city_selected for details  

Phone number is change from a number to string  
This change was made to accomodate for different formats eg. 123-45-67 vs 123/45/67 vs 1234567  

# Validation
Simple validators added to UI elements using QRegExpValidator  
The end-user is still free to add silly/incorrect information, such as a blank input for name  
However, it's not possible to add letters to phone numbers, for example  
Check add_validation() function for more details  

