#ifndef ADDRESS_H
#define ADDRESS_H

#include <QString>

class address
{
public:
    address(QString city = "", unsigned postal_code = 0, QString state = "", QString street_address = "");

    QString city;
    int postal_code;
    QString state;
    QString street_address;
};

#endif // ADDRESS_H
