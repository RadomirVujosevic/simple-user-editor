#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QVector>
#include "user.h"
#include "add_user_form.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

public slots:
    //Is triggered when a new user is clicked in the users list widget, or when a user is removed
    //The purpose of the function is to update the UI. More specifically, it updates the labels on the right side of the UI.
    void new_user_selected(int current_row);

    void open_new_user_form();
    void new_user_added(user new_user);
    void registration_cancelled();
    void user_removed();

private:
    Ui::Widget *ui;
    add_user_form *new_user_form = nullptr;
    QVector<user> *users;

    void add_user_to_ui(user &new_user);
    void read_user_data();
    void write_users_to_file();

};
#endif // WIDGET_H
