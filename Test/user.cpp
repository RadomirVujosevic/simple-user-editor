#include "user.h"

user::user(QString firstname, QString lastname, QString gender, QString date_of_birth, address user_address, QVector<phone_number> numbers)
    : firstname(firstname), lastname(lastname), gender(gender), date_of_birth(date_of_birth), user_address(user_address), numbers(numbers)
{

}

QString user::toQString(){
    return QString("firstname: %1\nlastname: %2\ngender: %3\nDate of birth: %4\n").arg(firstname, lastname, gender, date_of_birth);
}
