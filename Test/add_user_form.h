#ifndef ADD_USER_FORM_H
#define ADD_USER_FORM_H

#include <QWidget>
#include "user.h"
#include <QHash>

namespace Ui {
class add_user_form;
}

class add_user_form : public QWidget
{
    Q_OBJECT

public:
     add_user_form(QWidget *parent = nullptr);
    ~add_user_form();

signals:
    void new_user_added_signal(user new_user);
    void registration_cancelled();

private slots:
    void new_user_added_slot();
    void new_city_selected(int current_index);

    //This form's ui is private so it can't be connected with Widget(main form) directly
    //This slot serves as an intermediary => cancelButtonClick -> cancel_registration() emits signal -> registration_cancelled() in Widget
    void cancel_registration();

private:
    void add_validation();
    void setup_cb_city();
    QHash<QString, QString> *city_state_map;
    Ui::add_user_form *ui;
};

#endif // ADD_USER_FORM_H
