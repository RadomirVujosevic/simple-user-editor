#include "widget.h"
#include "./ui_widget.h"

#include <QDebug>
#include <QJsonArray>
#include <QStringList>
#include <QJsonObject>
#include <iostream>
#include <QFile>
#include <QJsonDocument>
#include <QListWidget>
#include <QPushButton>
#include <QMessageBox>

#include "phone_number.h"


Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    users =new QVector<user>();
    read_user_data();

    connect(ui->lw_users, &QListWidget::currentRowChanged, this, &Widget::new_user_selected);
    connect(ui->pb_add_user, &QPushButton::pressed, this, &Widget::open_new_user_form);
    connect(ui->pb_remove_user, &QPushButton::pressed, this, &Widget::user_removed);

}

void Widget::user_removed(){

    int selected_user_index = ui->lw_users->currentRow();

    //SPECIAL CASE: this only happens when the end-user clicks remove and there are no users in the list at the moment
    if(selected_user_index == -1) return;

    const user &selected_user = users->at(selected_user_index);
    QMessageBox::StandardButton confirmation_window;
    auto reply = QMessageBox::question(this, "Confirmation",
                                       QString("Are you sure you want to delete %1 %2").arg(selected_user.firstname, selected_user.lastname),
                                       QMessageBox::Yes|QMessageBox::No
                                       );
    if (reply == QMessageBox::Yes){
        delete ui->lw_users->takeItem(selected_user_index);
        users->remove(selected_user_index);
        write_users_to_file();
    }

}

void Widget::write_users_to_file(){

    //Creating Json object from user array
    QJsonArray users_array;
    for (auto it = users->begin(); it != users->end(); it++){

        user &user = *it;
        address &user_address = user.user_address;
        QJsonObject address_object;
        address_object.insert("city", user_address.city);
        address_object.insert("postalCode", user_address.postal_code);
        address_object.insert("state", user_address.state);
        address_object.insert("streetAddress", user_address.street_address);

        QVector<phone_number> &numbers = user.numbers;
        QJsonArray phone_number_array;
        for(auto &number: numbers){
            QJsonObject phone_number_object;
            phone_number_object.insert("number", number.number);
            phone_number_object.insert("type", number.type);
            phone_number_array.append(phone_number_object);
        }

        QJsonObject user_object;
        user_object.insert("address", address_object);
        user_object.insert("dateOfBirth", user.date_of_birth);
        user_object.insert("firstName", user.firstname);
        user_object.insert("gender", user.gender);
        user_object.insert("lastName", user.lastname);
        user_object.insert("phoneNumbers", phone_number_array);

        users_array.append(user_object);
    }

    //Writing object to file
    QJsonDocument users_doc;
    users_doc.setArray(users_array);
    QByteArray bytes = users_doc.toJson(QJsonDocument::Indented);
    QFile file("../Assets/users.json");
    if (file.open(QFile::WriteOnly | QFile::Truncate)){
        QTextStream iStream(&file);
        iStream.setCodec("utf-8");
        iStream << bytes;
        file.close();
    }else {
        std::cout << "file open failed: " << std::endl;
    }

}

void Widget::open_new_user_form(){
    //The form is hidden so it may appear that this check doesn't need to happen
    //The problem is that its possible to very quickly press the Add button twice, oppenning 2 forms and one of them is a memory leak.
    if(new_user_form == nullptr){
        new_user_form = new add_user_form();
        new_user_form->show();

        connect(new_user_form, &add_user_form::new_user_added_signal, this, &Widget::new_user_added);
        connect(new_user_form, &add_user_form::registration_cancelled, this, &Widget::registration_cancelled);

        hide();
    }
}

void Widget::new_user_added(user new_user){
    users->push_back(new_user);
    add_user_to_ui(new_user);
    write_users_to_file();
    show();
    delete new_user_form;
    new_user_form = nullptr;
}

void Widget::registration_cancelled(){

    show();
    delete new_user_form;
    new_user_form = nullptr;
}

void Widget::new_user_selected(int current_row){
    //SPECIAL CASE: when the first user in the list is removed (with index 0), this function will be called with argument -1
    if(current_row == -1 ) return;

    const user &current_user= users->at(current_row);
    ui->lbl_address->setText(current_user.user_address.street_address);
    ui->lbl_city->setText(current_user.user_address.city);
    ui->lbl_date_of_birth->setText(current_user.date_of_birth);
    ui->lbl_firstname->setText(current_user.firstname);
    ui->lbl_gender->setText(current_user.gender);
    ui->lbl_lastname->setText(current_user.lastname);
    ui->lbl_state->setText(current_user.user_address.state);
    ui->lbl_postal_code->setText(QString::number(current_user.user_address.postal_code));


    ui->lw_phone->clear();
    for(auto &number: current_user.numbers){
        QString format_number = QString("%1 : %2").arg(number.type, number.number);
        ui->lw_phone->addItem(format_number);
    }
}

void Widget::add_user_to_ui(user &new_user){
    QString new_widget_item = QString("%1 %2").arg(new_user.firstname, new_user.lastname);
    ui->lw_users->addItem(new_widget_item);
}

void Widget::read_user_data(){
    QFile user_file("../Assets/users.json");
    user_file.open(QFile::ReadWrite);

    auto file_contents = user_file.readAll();
    user_file.close();
    QJsonDocument users_json = QJsonDocument::fromJson(file_contents);
    if (users_json.isNull()){
        std::cout << "File parsing failed" << std::endl;
        return;
    }

    QJsonArray user_array = users_json.array();
    for(auto it = user_array.cbegin(); it != user_array.cend(); it++){
        QJsonObject user_object = it->toObject();


        user new_user = user();
        new_user.firstname = user_object.value("firstName").toString();
        new_user.lastname = user_object.value("lastName").toString();
        new_user.gender = user_object.value("gender").toString();
        new_user.date_of_birth = user_object.value("dateOfBirth").toString();

        QJsonObject address_object = user_object.value("address").toObject();
        new_user.user_address.city = address_object.value("city").toString();
        new_user.user_address.state = address_object.value("state").toString();
        new_user.user_address.street_address = address_object.value("streetAddress").toString();
        new_user.user_address.postal_code = address_object.value("postalCode").toInt();

        QJsonArray phone_array = user_object.value("phoneNumbers").toArray();
        for(auto it2 = phone_array.cbegin(); it2 != phone_array.cend(); it2++ ){
            QJsonObject phone_object = it2->toObject();

            phone_number new_user_number = phone_number();
            new_user_number.type = phone_object.value("type").toString();
            new_user_number.number = phone_object.value("number").toString();
            new_user.numbers.push_back(new_user_number);
        }
        users->push_back(new_user);

    }

    for(auto it = users->begin(); it != users->end(); it++){
        add_user_to_ui(*it);
    }

}

Widget::~Widget()
{
    delete users;
    delete ui;
}

