#ifndef PHONE_H
#define PHONE_H

#include <QString>

class phone_number
{
public:
    phone_number(QString number = "", QString type="");

    //Number is string in order to accept multiple formats - +3801234567 vs +380 123 45 67 vs +380-123-45-67
    QString number;
    QString type;
};

#endif // PHONE_H
