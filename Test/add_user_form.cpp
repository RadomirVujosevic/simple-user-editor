#include "add_user_form.h"
#include "ui_add_user_form.h"

#include <iostream>

#include <QPushButton>
#include <QRegExpValidator>
#include <QRegExp>
#include <QComboBox>
#include <QVector>

add_user_form::add_user_form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::add_user_form)
{
    ui->setupUi(this);

    connect(ui->pb_ok, &QPushButton::pressed, this, &add_user_form::new_user_added_slot);
    connect(ui->cb_city, qOverload<int>(&QComboBox::activated), this, &add_user_form::new_city_selected);
    connect(ui->pb_cancel, &QPushButton::pressed, this, &add_user_form::cancel_registration);

    setup_cb_city();
    add_validation();
}

//The idea is to keep City consistent with State.
//Instead of making State a Line edit so the end-user can input their own state, cities and states are kept in a map and State is a QLable to make it clear that it can't be edited
void add_user_form::setup_cb_city(){
    city_state_map = new QHash<QString, QString>();

    city_state_map->insert(QString("New York"), QString("NY"));
    city_state_map->insert(QString("Washington"), QString("DC"));
    city_state_map->insert(QString("Los Angeles"), QString("CA"));
    city_state_map->insert(QString("San Jose"), QString("CA"));
    city_state_map->insert(QString("Belgrade"), QString("SR"));

    auto keys = city_state_map->keys();
    for(auto& key:  keys){
        ui->cb_city->addItem(key);
    }
    //Sets the text of State QLabel to the state of first city(index 0) according to city_state_map
    //With this implementation, the order of the cities is random, so the first city can't be predetrmined
    //Could be implemented in a non-random way but this function has a very low cost
    new_city_selected(0);
}

add_user_form::~add_user_form()
{
    delete city_state_map;
    delete ui;

}

void add_user_form::new_user_added_slot(){
    //Collects all of the data about the user from the UI and makes a new user, which is sent via signal to main form

    QString firstname = ui->le_firstname->text();
    QString lastname = ui->le_lastname->text();
    QString street_address = ui->le_address->text();
    QString gender = ui->cb_gender->currentText();
    unsigned post_code = ui->le_post_code->text().toUInt();
    QString state = ui->lbl_state->text();
    QString home_phone = ui->le_home_phone->text();
    QString mobile_phone = ui->le_mobile_phone->text();
    QString city = ui->cb_city->currentText();
    QString date = ui->de_date_of_birth->text();

    address new_user_address(city, post_code, state, street_address);
    QVector<phone_number> new_user_numbers{};
    if(home_phone != "") new_user_numbers.push_back(phone_number(home_phone, "home"));
    if(mobile_phone != "") new_user_numbers.push_back(phone_number(mobile_phone, "mobile"));

    user new_user = user(firstname, lastname, gender, date, new_user_address, new_user_numbers);
    emit new_user_added_signal(new_user);
}



void add_user_form::new_city_selected(int current_index){
    //Select the current city from combo box
    QString current_city = ui->cb_city->currentText();
    //Map it to it's state
    QString current_state = city_state_map->value(current_city);
    //Set the state as new text in the State QLabel
    ui->lbl_state->setText(current_state);
}


void add_user_form::cancel_registration(){
    emit registration_cancelled();
}

void add_user_form::add_validation(){

    //Adresses can accept letters, number, empty spaces or '.'
    QRegExp address_regex = QRegExp("[ a-zA-Z\\d\\.]+");
    ui->le_address->setValidator(new QRegExpValidator(address_regex));

    //Names only accept letters and empty spaces
    QRegExp name_regex = QRegExp("[ a-zA-Z]+");
    ui->le_firstname->setValidator(new QRegExpValidator(name_regex));
    ui->le_lastname->setValidator(new QRegExpValidator(name_regex));


    //Numbers can accept numbers, empty spaces, '-' and '/' characters.
    QRegExp phone_regex = QRegExp("[\\d -/]+");
    ui->le_home_phone->setValidator(new QRegExpValidator(phone_regex));
    ui->le_mobile_phone->setValidator(new QRegExpValidator(phone_regex));

    //postal code is a number between 10,000 and 100,000
    ui->le_post_code->setValidator(new QIntValidator(10000, 99999));
}


