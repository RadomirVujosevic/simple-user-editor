#ifndef USER_H
#define USER_H

#include <QString>
#include <QVector>
#include "address.h"
#include "phone_number.h"

class user
{
public:
    user(QString firstname = "",
         QString lastname = "",
         QString gender = "",
         QString date_of_birth = "",
         address user_address = address(),
         QVector<phone_number> numbers = QVector<phone_number>());

    address user_address;
    QVector<phone_number> numbers;



    QString date_of_birth;
    QString firstname;
    QString lastname;
    QString gender;

    QString toQString();
};

#endif // USER_H
